/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author alumno
 */
public class Pag {

    int day;
    int month;
    boolean holy;
    LinkedList<cita> citas = new LinkedList<>();

    public Pag(int day, int month, LinkedList<cita> citas) {
        this.day = day;
        this.month = month;
        this.holy = true;
        this.citas = citas;
    }
    public Pag() {}

    ;
    
    
    public void insertarCita(Agendas a) {
        boolean correcto = false;
        String meses = "";
        int pagina;
        do {
            try {
                String mesCita = "¿Para que mes quieres la cita";
                Scanner sc = new Scanner(System.in);
                pagina = sc.nextInt();

                month = Integer.parseInt(mesCita);
               
                if ((month >= 1) && (month <= 12)) {
                    System.out.println("Selecciona el día");                    
                    day = Integer.parseInt(diaCita);
                    
                    switch (month) {
                        case 1:
                            if ((day >= 1) && (day <= 31) && month==1) {
                                correcto = true;
                                meses = "Enero";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 2:
                            if ((day >= 1) && (day <= 28)&& month==2) {
                                correcto = true;
                                meses = "Febrero";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 3:
                            if ((day >= 1) && (day <= 31)&& month==3) {
                                correcto = true;
                                meses = "Marzo";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 4:
                            if ((day >= 1) && (day <= 30)&& month==4) {
                                correcto = true;
                                meses = "Abríl";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 5:
                            if ((day >= 1) && (day <= 31)&& month==5) {
                                correcto = true;
                                meses = "Mayo";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 6:
                            if ((day >= 1) && (day <= 30)&& month==6) {
                                correcto = true;
                                meses = "Junio";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 7:
                            if ((day >= 1) && (day <= 31)&& month==7) {
                                correcto = true;
                                meses = "Julio";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 8:
                            if ((day >= 1) && (day <= 31)&& month==8) {
                                correcto = true;
                                meses = "Agosto";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 9:
                            if ((day >= 1) && (day <= 30)&& month==9) {
                                correcto = true;
                                meses = "Septiembre";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 10:
                            if ((day >= 1) && (day <= 31)&& month==10) {
                                correcto = true;
                                meses = "Octubre";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 11:
                            if ((day >= 1) && (day <= 30)&& month==11) {
                                correcto = true;
                                meses = "Noviembre";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;

                        case 12:
                            if ((day >= 1) && (day <= 31)&& month==12) {
                                correcto = true;
                                meses = "Diciembre";
                            } else {
                                System.out.println("Lo siento, los datos introducidos no son correctos");
                            }
                            break;
                    }
                } else {
                    System.out.println("Lo siento, los datos introducidos no son correctos");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (correcto == false);

        System.out.println("Has introducido una cita para el dia " + day + " de " + meses);

        cita c1 = new cita();

        c1.crearCita();

        citas.add(c1);

        Pag p = new Pag(day, month, citas);

        a.paginas.add(p);

        a.Agenda();

    }
    */

    public void vaciarAgenda(Agendas a) {

        if (a.paginas.isEmpty() == false) {

            a.paginas.clear();
            System.out.println("Has limpiado toda la agenda");

        } else {
            System.out.println("La agenda no tiene citas");
        }

        a.Agenda();
    }

}
