/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicathreads;

import java.util.Stack;

public class Hilo extends Thread {

    Stack<Integer> numbers;

    public Hilo(Stack<Integer> numbers) {
        this.numbers = numbers;
    }

    //en el while procesamos el numero 
    public void run() {
        while (!numbers.isEmpty()) {
            Integer number = numbers.pop();
            //procesarNumero(number);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hemos procesado numero: " + number);
        }
    }
}
