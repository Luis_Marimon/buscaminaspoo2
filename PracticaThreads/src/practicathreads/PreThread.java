package practicathreads;

import java.util.Date;
import java.util.Stack;

public class PreThread {

    /**
     *
     * @author Luis
     */

    public static void main(String[] args) {
        Stack<Integer> numbers = new Stack<Integer>();
        //Rellenamos pila
        for (int i = 0; i < 1000; i++) {
            numbers.push(i);
        }
        long tiempoInicial = new Date().getTime();

        Hilo Hilo1 = new Hilo(numbers);
        Hilo Hilo2 = new Hilo(numbers);
        Hilo Hilo3 = new Hilo(numbers);
        Hilo Hilo4 = new Hilo(numbers);
        Hilo Hilo5 = new Hilo(numbers);

        Hilo1.start();
        Hilo2.start();
        Hilo3.start();
        Hilo4.start();
        Hilo5.start();

        System.out.println("Ha tardado " + (new Date().getTime()
                - tiempoInicial) / 1000 + "s");
    }

}
