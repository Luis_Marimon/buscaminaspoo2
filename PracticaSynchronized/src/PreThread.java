/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Luis
 */
import java.util.Date;
import java.util.Stack;

public class PreThread {

    static int[] numbers = new int[1000];
    static int nextNumbreToProcess = 0;

    public static void main(String[] args) {

        //Rellenamos array
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = i;

        }

        long tiempoInicial = new Date().getTime();

        Hilo Hilo1 = new Hilo(numbers);
        Hilo Hilo2 = new Hilo(numbers);
        Hilo Hilo3 = new Hilo(numbers);
        Hilo Hilo4 = new Hilo(numbers);
        Hilo Hilo5 = new Hilo(numbers);

        Hilo1.start();
        Hilo2.start();
        Hilo3.start();
        Hilo4.start();
        Hilo5.start();
        try {
            Hilo1.join();
            Hilo2.join();
            Hilo3.join();
            Hilo4.join();
            Hilo5.join();

        } catch (Exception e) {
        }

        System.out.println("Ha tardado " + (new Date().getTime()
                - tiempoInicial) / 1000 + "s");

    }
//con sync crea una cola
    public static synchronized int syncindex() {
        
        return nextNumbreToProcess++;
        
    }
}
