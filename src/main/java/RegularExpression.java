
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpression {

    public static void main(String[] args) {

        String mydata = "'string' with 'the data i want' inside";
        Pattern pattern = Pattern.compile("img src\\\\s*=\\\\s*\\\" (.+?)\\\" \"");
        Matcher matcher = pattern.matcher(mydata);

        while (matcher.find()) {
            System.out.println(matcher.group(1));
        }

    }

}
