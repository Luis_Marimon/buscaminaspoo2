/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threadpoolexecutor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author alumno
 */
public class ThreadPoolExecutorDos {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        List<Callable> calables = new ArrayList();
        //For rellena 1000  
        for (int i = 1; i <= 1000; i++) {
            //Si cambiamos el false por el True el hilo se dormira 1000ms
            calables.add(new Squaring(i, false));
        }
        //EXECUTOR CON LOS HILOS NECESARIOS ==> 15-20ms
        ExecutorService executor = Executors.newCachedThreadPool();
        System.out.println("Executor");
        long time = System.currentTimeMillis();
        List<Future<Integer>> futures = executor.invokeAll((Collection) calables);
        //Recorre la lista de Futures, obtenemos los valores
        for (Future future : futures) {
            future.get();
        }
        //Tiempo y Shutdown
        System.out.println("TIME :" + (System.currentTimeMillis() - time));
        executor.shutdown();
    }
}
