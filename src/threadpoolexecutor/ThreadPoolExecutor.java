/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threadpoolexecutor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author alumno
 */
public class ThreadPoolExecutor {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        List<Callable> calables = new ArrayList();
        //For rellena 1000  
        for (int i = 1; i <= 1000; i++) {
            /*1 y el 2 SIN espera: la operacion es tan basica que al crear los hilos en el 2
             el 1 ya ha hecho las operaciones.
             1 y el 2 CON espera: el 1 hace 2 operaciones por segundo 
             mientras el 2 tarda 1s y hace todos los hilos=tareas
             */
            //Si cambiamos el false por el true el hilo se dormira 1000ms
            calables.add(new Squaring(i, true));
        }
        //EXECUTOR CON 2 HILOS FIJOS ==>  5-10ms 
        ExecutorService executor = Executors.newFixedThreadPool(2);
        System.out.println("Executor");
        long time = System.currentTimeMillis();
        List<Future<Integer>> futures = executor.invokeAll((Collection) calables);
        //Recorre la lista de Futures, obtenemos los valores
        for (Future future : futures) {
            future.get();
        }
        //Tiempo y Shutdown
        System.out.println("TIME :" + (System.currentTimeMillis() - time));
        executor.shutdown();
    }

}
