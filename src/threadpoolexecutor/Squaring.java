/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threadpoolexecutor;

import java.util.concurrent.Callable;

/**
 *
 * @author alumno
 */
public class Squaring implements Callable<Integer> {

    private Integer base;
    //El Booleano se activa en la practica 3 para que espere 1000ms.
    private boolean wait;

    public Squaring(Integer base, boolean wait) {
        this.wait = wait;
        this.base = base;
    }

    @Override
    public Integer call() throws Exception {

        if (wait == true) {
            Thread.sleep(1000);
        }
        return base * base;
    }
}
