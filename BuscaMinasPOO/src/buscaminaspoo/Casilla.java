package buscaminaspoo;

/**
 *
 * @author Luis
 */
public class Casilla {
//Type -> True == Mina  False == Vacia

    private boolean type;
    private boolean open;
    private int numMinas;

    public Casilla(boolean type) {
        this.type = type;
        this.open = false;

    }

    public int getNumMinas() {
        return this.numMinas;
    }

    public void setNumMinas(int numMinas) {
        this.numMinas = numMinas;
    }

    public boolean isOpen() {
        return open;
    }

    public boolean isType() {
        return type;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public void addMines(int numM) {
        this.numMinas = this.numMinas + numM;
    }

    void open() {
        setOpen(true);
    }

}
