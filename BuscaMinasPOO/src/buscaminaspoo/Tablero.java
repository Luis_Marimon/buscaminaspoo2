/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaminaspoo;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Luis
 *
 *
 */
public class Tablero {

    private int lines = 10, columns = 10;
    private Casilla[][] board;
    private static boolean win;
    private static boolean end;
    Scanner sc = new Scanner(System.in);
    Random random = new Random();
    Scanner input = new Scanner(System.in);

    Tablero() {
        board = new Casilla[lines][columns];
        startMines();
        runArray();
        game();
    }
    ///////////MINAS////////////
    private void startMines() {
        int x, y;
        Vacia vacia;
        Mina mina = new Mina(true);
        Random rn = new Random();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                vacia = new Vacia(false);
                board[i][j] = vacia;
            }
        }

        for (int i = 0; i < 10; i++) {
            x = rn.nextInt(10);
            y = rn.nextInt(10);
            System.out.println(x + " " + y);
            if (!board[x][y].isType()) {
                board[x][y] = mina;
            } else {
                i--;
            }

        }
    }

    private void runArray() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                board[i][j].addMines(setNumMines(i, j));
            }
        }
    }

    private int setNumMines(int posX, int posY) {
        int aux = 0;
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if ((i + posX > 0 && i + posX < 10) && (j + posY > 0 && j + posY < 10)) {
                    if (board[i + posX][j + posY].isType()) {
                        aux++;
                    }
                }
            }
        }
        return aux;
    }

    private void check(int x, int y) {
        if (board[x][y].isType()) {
            end = true;
            printBoard();
        } else {
            board[x][y].setOpen(true);
            game();
        }
    }

    //debugmode - no using
    private void abrirTodo() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                board[i][j].setOpen(true);
            }
        }
    }

////////////TABLERO////////////
    private void printBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j].isOpen()) {

                    if (board[i][j].isType()) {
                        System.out.print("o");
                    } else {
                        System.out.print(board[i][j].getNumMinas());
                    }
                } else {
                    System.out.print("[]");
                }
            }
            System.out.println("");
        }
    }
////////////GAME////////////

    private void game() {
        while (!end) {
            printBoard();
            System.out.println("Coord X");
            int x = sc.nextInt();
            System.out.println("Coord Y");
            int y = sc.nextInt();
            board[x][y].open();
            check(x, y);
        }
    }

}
