
import java.awt.Graphics;

/**
 *
 * @author Luis
 */
public interface FigureFill extends Figure {

    @Override
    public void paint(Graphics graphics);

    public void fill(Graphics graphics);

    public int rellenar();

}
