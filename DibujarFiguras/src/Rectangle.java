
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Luis
 */
public class Rectangle implements FigureFill {

    private Color color;
    private final int px, py, px2, py2;
    private int rellena = 0;

    public Rectangle(int x, int y, int height, int high, int rellena, Color color) {
        this.px = x;
        this.py = y;
        this.px2 = height;
        this.py2 = high;
        this.rellena = rellena;
        this.color = color;

    }

    @Override
    public void paint(Graphics graphics) {

        graphics.drawRect(px, py, px2, py2);

    }

    @Override
    public void fill(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillRect(px, py, px2, py2);
    }

    @Override
    public int rellenar() {
        return rellena;
    }

}
