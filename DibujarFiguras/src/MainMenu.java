
import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author Luis
 */
public class MainMenu extends JFrame {

    private static int x;
    private static int y;
    private static int x2;
    private static int y2;
    private static final Scanner sc = new Scanner(System.in);
    private static int idf = -1;
    private static int fill;
    private static int sides;
    private static int[] cx;
    private static int[] cy;
    private static String chain;
    private static Color color = Color.red;

    public static void main(String[] args) {
        final Panel panel = new Panel();
        Runnable guiThread = (new Runnable() {
            public void run() {
                JFrame frame;

                //Create and set up the window.
                frame = new JFrame("FrameDemo");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                frame.setBounds(100, 100, 800, 600);
                frame.getContentPane().add(panel);

                //Display the window.
                frame.setVisible(true);
            }
        });

        try {
            SwingUtilities.invokeAndWait(guiThread);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("1. Cadena de Texto");
        System.out.println("2. Punto");
        System.out.println("3. Linea");
        System.out.println("4. Rectangulo");
        System.out.println("5. Cuadrado");
        System.out.println("6. Circulo");
        System.out.println("7. Poligono");
        System.out.println("8. Historial de figuras");
        System.out.println("9. Salir");

        idf = sc.nextInt();
        switch (idf) {
            case 1:
                System.out.println("Cadena de texto");
                System.out.println("Define distancia horizontal");
                System.out.println("X");
                x = sc.nextInt();
                System.out.println("Y");
                y = sc.nextInt();
                System.out.println("Escibre el texto a imprimir:");
                sc.nextLine();
                chain = sc.nextLine();
                panel.setFigure(new Text(x, y, chain));
                panel.setVisible(true);
                panel.repaint();
                break;
            case 2:
                System.out.println("Punto");
                System.out.println("/////////////////////");
                fill = 1;
                System.out.println("Define distancia horizontal");
                System.out.println("X");
                x = sc.nextInt();
                System.out.println("Define distancia vertical");
                System.out.println("Y");
                y = sc.nextInt();
                panel.setFigurefill(new Point(x, y, fill, color));
                panel.setVisible(true);
                panel.repaint();
                break;
            case 3:
                System.out.println("Linea");
                System.out.println("/////////////////////");
                fill = 0;
                System.out.println("Define el primer punto:");
                System.out.println("X");
                x = sc.nextInt();
                System.out.println("Y");
                y = sc.nextInt();
                System.out.println("Define el segundo punto de union:");
                System.out.println("X2");
                x2 = sc.nextInt();
                System.out.println("Y2");
                y2 = sc.nextInt();
                panel.setFigure(new Line(x, y, x2, y2));
                panel.setVisible(true);
                panel.repaint();
                break;
            case 4:
                System.out.println("Rectangulo");
                System.out.println("Lo quieres pintar? 0=no 1=sí");
                fill = sc.nextInt();
                System.out.println("/////////////////////");
                System.out.println("Define el primer punto:");
                System.out.println("X");
                x = sc.nextInt();
                System.out.println("Y");
                y = sc.nextInt();
                System.out.println("Define heigh y high");
                System.out.println("HEIGH");
                x2 = sc.nextInt();
                System.out.println("HIGH");
                y2 = sc.nextInt();
                if (fill == 0) {
                    panel.setFigure(new Rectangle(x, y, x2, y2, fill, color));
                } else {
                    panel.setFigurefill(new Rectangle(x, y, x2, y2, fill, color));
                }
                panel.setVisible(true);
                panel.repaint();
                break;
            case 5:
                System.out.println("Cuadrado");
                System.out.println("Lo quieres pintar? 0=no 1=sí");
                fill = sc.nextInt();
                System.out.println("/////////////////////");
                System.out.println("Define el primer punto:");
                System.out.println("X");
                x = sc.nextInt();
                System.out.println("Y");
                y = sc.nextInt();
                System.out.println("Define la medida del lado");
                System.out.println("SIDE");
                x2 = sc.nextInt();
                if (fill == 0) {
                    panel.setFigure(new Square(x, y, x2, fill, color));
                } else {
                    panel.setFigurefill(new Square(x, y, x2, fill, color));
                }
                panel.setVisible(true);
                panel.repaint();
                break;
            case 6:
                System.out.println("Circulo");
                System.out.println("Lo quieres pintar? 0=no 1=si");
                fill = sc.nextInt();
                System.out.println("/////////////////////");
                System.out.println("Define el centro:");
                System.out.println("X");
                x = sc.nextInt();
                System.out.println("Y");
                y = sc.nextInt();
                System.out.println("RADIO");
                x2 = sc.nextInt();
                if (fill == 0) {
                    panel.setFigure(new Circle(x, y, x2, fill, color));
                } else {
                    panel.setFigurefill(new Circle(x, y, x2, fill, color));
                }
                panel.setVisible(true);
                panel.repaint();
                break;
            case 7:
                System.out.println("Poligono");
                System.out.println("Lo quieres pintar? 0=no 1=sí");
                fill = sc.nextInt();
                System.out.println("Define numero de lados:");
                sides = sc.nextInt();
                cx = new int[sides];
                cy = new int[sides];
                for (int i = 0; i < sides; i++) {
                    System.out.println("Define el primer punto");
                    System.out.println("X");
                    cx[i] = sc.nextInt();
                    System.out.println("Define el segundo punto");
                    System.out.println("Y");
                    cy[i] = sc.nextInt();
                }
                if (fill == 0) {
                    panel.setFigure(new Poligon(sides, cx, cy));
                } else {
                    panel.setFigurefill(new Poligon(sides, cx, cy));
                }
                break;
            case 8:
                System.out.println("Historial de figuras");

                break;
            case 9:
                System.out.println("Salir");
                break;
            default:
                System.out.println("Elige una opcion correcta");
                break;
                
        }

    }

}
