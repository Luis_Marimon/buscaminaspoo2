
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Luis
 */
public class Point implements FigureFill {

    private final int px, py, px2;
    private Color color;
    private int rellena = 0;

    public Point(int x, int y, int rellena, Color color) {
        this.px = x;
        this.py = y;
        this.px2 = 3;
        this.rellena = rellena;
        this.color = color;
    }

    @Override
    public void paint(Graphics graphics) {

        graphics.drawOval(px, py, px2, px2);

    }

    @Override
    public void fill(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillOval(px, py, px2, px2);

    }

    @Override
    public int rellenar() {
        return rellena;
    }

}
