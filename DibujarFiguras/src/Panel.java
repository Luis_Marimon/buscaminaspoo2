
import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Luis
 */
public class Panel extends JPanel {

    //private List<Figure> figure;
    private Figure figure;
    private FigureFill figurefill;

    @Override
    public void paint(Graphics g) {
        if (figure != null) {
            figure.paint(g);
        }

        if (figurefill != null) {
            figurefill.fill(g);
        }

    }

    public void setFigure(Figure figure) {
        this.figure = figure;
    }

    /**
     * @param figurefill the figurefill to set
     */
    public void setFigurefill(FigureFill figurefill) {
        this.figurefill = figurefill;
    }
}
