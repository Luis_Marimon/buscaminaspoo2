
import java.awt.Graphics;

/**
 *
 * @author Luis
 */
public class Text implements Figure {

    private int px, py;
    private String chain;

    public Text(int x, int y, String chain) {
        this.px = x;
        this.py = y;
        this.chain = chain;
    }

    @Override
    public void paint(Graphics g) {
        g.drawString(chain, px, py);
    }

}
